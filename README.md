Overview
========================
Spire.DocViewer for .NET, a professional and innovative Word viewer library, enables developers to create any WinForm applications to open, view, print and convert Word document in C# and Visual Basic for .NET (2.0, 3.5, 4.0 and 4.0 ClientProfile).

Functions
=====================
Open and View

Spire.DocViewer for .NET enables developers to load and display Word document contents in Form or open specified document from directory. Also, developers can view Word with zoom in/out, page up/down, moving to the first/last and specified page, fitting page/width. 

Print 

Spire.DocViewer for .NET enables developers to print Word document with variety of settings, such as margins, page orentation, page size etc. Also, users can choose to print current page, specified one, multiple pages or all pages. 

Objects

Spire.DocViewer for .NET supports variety of document objects, including Text(font, color, size, style, positon), image (position, height, width), paragraph(alignment, spacing, indent), table(row height, column width, border and shading) and bookmark. 

Convert

Spire.DocViewer for .NET eanbles developers to convert Word to PDF, HTML and RTF quickly with high fidelity. Also, conversion between Word different versions (.doc and .docx) is supported. 

For more information about Spire.DocViewer for .NET: http://www.e-iceblue.com/Introduce/word-viewer-net-introduce.html

Free Trial: http://www.e-iceblue.com/Download/download-docviewer-for-net-now.html
